-- Color Scheme
require("nebulous").setup {
  variant = "night",
  disable = {
    background = true,
    endOfBuffer = false,
    terminal_colors = false,
  },
  italic = {
    comments   = false,
    keywords   = false,
    functions  = false,
    variables  = false,
  },
  custom_colors = { -- this table can hold any group of colors with their respective values
    -- LineNr = { fg = "#5BBBDA", bg = "NONE", style = "NONE" },
    LineNr = { fg = "magenta", bg = "NONE", style = "NONE" },
    CursorLineNr = { fg = "#E1CD6C", bg = "NONE", style = "NONE" },
    -- Comment = { fg = "#EA6739", bg = "NONE", style = "NONE" },
    Comment = { fg = "#5BBBDA", bg = "NONE", style = "NONE" },

    -- it is possible to specify only the element to be changed
    TelescopePreviewBorder = { fg = "#A13413" },
    LspDiagnosticsDefaultError = { bg = "#E11313" },
    TSTagDelimiter = { style = "bold,italic" },
  }
}

-- temp fix for comment color because of treesitter highlighting overwriting some highlight groups
vim.api.nvim_set_hl(0, '@comment', {fg = '#5BBBDA' })
vim.api.nvim_set_hl(0, 'TabLine', {fg = 'white', bg='grey' })
-- vim.api.nvim_set_hl(0, 'TabLineSel', {fg = '#0b1015', bg='#ff8d03'})
-- vim.api.nvim_set_hl(0, 'TabLineFill', {cterm = 'magenta' })

-- Statusbar Color Scheme
require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'nebulous',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {
      statusline = {},
      winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = false,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    }
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {}
}

-- lightline
-- vim.g.lightline = {
--   -- colorscheme = 'onedark',
--   colorscheme = 'deus',
--   active = { left = { { 'mode', 'paste' }, { 'gitbranch', 'readonly', 'filename', 'modified' } } },
--   component_function = { gitbranch = 'FugitiveHead' },
-- }
