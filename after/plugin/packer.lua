-- Packer Bootstrap
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

-- local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

-- if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
--   vim.fn.execute('!git clone https://github.com/wbthomason/packer.nvim ' .. install_path)
-- end

-- vim.cmd [[
--   augroup Packer
--     autocmd!
--     autocmd BufWritePost init.lua PackerCompile
--   augroup end
-- ]]

-- local use = require('packer').use

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim' -- Package manager
  use 'tpope/vim-fugitive' -- Git commands in nvim
  use 'tpope/vim-rhubarb' -- Fugitive-companion to interact with github
  -- use 'tpope/vim-commentary' -- "gc" to comment visual regions/lines
  use {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  }
  use 'tpope/vim-surround' -- Edit pairs cs"'
  use 'windwp/nvim-autopairs'

  -- use 'ludovicchabant/vim-gutentags' -- Automatic ctags management

  -- TMUX Navigation
  use 'christoomey/vim-tmux-navigator'

  -- File Browser
  use 'stevearc/oil.nvim'

  -- THEMES
  use 'agude/vim-eldar' -- Eldar Theme
  use 'Yagua/nebulous.nvim' -- Nebulous theme
  -- use 'itchyny/lightline.vim' -- Fancier statusline
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  use 'joshdick/onedark.vim' -- Theme inspired by Atom

  use { 'nvim-telescope/telescope.nvim', requires = { 'nvim-lua/plenary.nvim' } } -- UI to select things (files, grep results, open buffers...)
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
  use 'lukas-reineke/indent-blankline.nvim' -- Add indentation guides even on blank lines
  use { 'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' } } -- Add git related info in the signs columns and popups
  use 'nvim-treesitter/nvim-treesitter' -- Highlight, edit, and navigate code using a fast incremental parsing library
  use { 'nvim-treesitter/nvim-treesitter-textobjects', run = ':TSUpdate' } -- Additional textobjects for treesitter

  use 'nvim-tree/nvim-web-devicons' -- Icons
  use 'windwp/nvim-ts-autotag'

  -- AI
  use 'Exafunction/codeium.vim'

  -- LANGUAGE SERVER
  use {
    'williamboman/mason.nvim',
    run = ':MasonUpdate' -- :MasonUpdate updates registry contents
  }
  use 'williamboman/mason-lspconfig.nvim'
  use 'neovim/nvim-lspconfig' -- Collection of configurations for built-in LSP client
  use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
  use 'hrsh7th/cmp-nvim-lsp' -- cmp source for neovim lua api
  use 'saadparwaiz1/cmp_luasnip'
  use 'L3MON4D3/LuaSnip' -- Snippets plugin

  --[[ Automatically configures lua-language-server for neovim ]]
  use 'folke/neodev.nvim'

  -- Dap (Debug Adapter Protocol)
  use { 'mfussenegger/nvim-dap', require = { use 'rcarriga/nvim-dap-ui' } }
  use 'theHamsta/nvim-dap-virtual-text'
  use 'nvim-telescope/telescope-dap.nvim'

  -- Debuggers
  -- use 'jbyuki/one-small-step-for-vimkind' -- Neovim Lua
  use 'mfussenegger/nvim-dap-python' -- Python

  -- Automatically set up configuration after cloning packer.nvim
  -- must be at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
