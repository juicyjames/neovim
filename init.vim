" set tabstop=2 softtabstop=2
" set shiftwidth=2
" set expandtab
set list
set nowrap
" set smartindent
set clipboard+=unnamedplus
set expandtab
set spelllang=en_au,en_gb
set nobackup
set noswapfile
set fileformats=unix,dos,mac

"" This is in lsp.lua
"" but also leave this here for now
" command! Scratch lua require('tools').makeScratch()

"" MY PLUGIN
"fun! MyPlugin()
"  " dont forget to remove this one....
"  " reload plugin on file change
"  lua for k in pairs(package.loaded) do if k:match("^my%-plugin") then package.loaded[k] = nil end end
"  lua require("my-plugin").createFloatingWindow()
"endfun
"
"augroup MyPlugin
"  autocmd!
"  autocmd VimResized * :lua require("my-plugin").onResize()
"augroup END

" auto window resize
augroup ReduceNoise
  autocmd!
  " Automatically resize actice split to 85 width
  autocmd WinEnter * :call ResizeSplits()
augroup END

function! ResizeSplits()
  if winwidth(0) <= 50
    set winwidth=20
    wincmd =
  endif
endfunction

""Debug Vim
"":profile start profile.log
"":profile func *
"":profile file *
""" At this point do slow actions
"":profile pause
"":noautocmd qall!

lua require'init'
