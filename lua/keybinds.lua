local opts = { noremap = true, silent = true }

--Remap space as leader key
vim.api.nvim_set_keymap('', '<Space>', '<Nop>', opts)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

--Remap for dealing with word wrap
vim.api.nvim_set_keymap('n', 'k', "v:count == 0 ? 'gk' : 'k'", { noremap = true, expr = true, silent = true })
vim.api.nvim_set_keymap('n', 'j', "v:count == 0 ? 'gj' : 'j'", { noremap = true, expr = true, silent = true })

-- Esc remap
vim.api.nvim_set_keymap('i', 'jj', '<ESC>', opts)

-- Spell checking
-- vim.api.nvim_set_keymap('i', '<leader>cs', 'C-X_C-L', opts)

-- Terminal
vim.api.nvim_set_keymap('t', 'jj', '<C-\\><C-n>', opts)
vim.api.nvim_set_keymap('n', '<leader>t', [[:term<CR>]], opts)
-- vim.api.nvim_set_keymap('n', '<leader>vt', [[:vs +term<CR>]], opts)
-- vim.api.nvim_set_keymap('n', '<leader>ht', [[:sp +term<CR>]], opts)

-- Wincmd
vim.api.nvim_set_keymap('n', '<leader>h', '<cmd>:wincmd h<CR>', opts)
vim.api.nvim_set_keymap('n', '<leader>j', '<cmd>:wincmd j<CR>', opts)
vim.api.nvim_set_keymap('n', '<leader>k', '<cmd>:wincmd k<CR>', opts)
vim.api.nvim_set_keymap('n', '<leader>l', '<cmd>:wincmd l<CR>', opts)
vim.api.nvim_set_keymap('n', '<leader>=', '<cmd>:wincmd =<CR>', opts)

-- Buffers
vim.api.nvim_set_keymap('n', '<leader>n', '<cmd>:tabnext<CR>', opts)
vim.api.nvim_set_keymap('n', '<leader>p', '<cmd>:tabprevious<CR>', opts)

-- Oil File Manager
vim.keymap.set('n', '-', require('oil').open, { desc = 'Open parent directory'})

-- Session
vim.api.nvim_set_keymap('n', '<leader>s', '<cmd>:mksession<CR>', opts)

-- Git
vim.api.nvim_set_keymap('n', '<leader>gp', "<cmd>Gitsigns preview_hunk_inline<cmd>", opts)

-- Debbugging for dap
vim.api.nvim_set_keymap('n', '<F5>', "<cmd>lua require'dap'.continue()<CR>", opts)
vim.api.nvim_set_keymap('n', '<F10>', "<cmd>lua require'dap'.step_over()<CR>", opts)
vim.api.nvim_set_keymap('n', '<F11>', "<cmd>lua require'dap'.step_into()<CR>", opts)
vim.api.nvim_set_keymap('n', '<F12>', "<cmd>lua require'dap'.step_out()<CR>", opts)
vim.api.nvim_set_keymap('n', '<leader>b', "<cmd>lua require'dap'.toggle_breakpoint()<CR>", opts)
vim.api.nvim_set_keymap('n', '<leader>B', "<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>", opts )
-- vim.api.nvim_set_keymap('n', '<leader>lp', "<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>", opts) -- interfers with window movements
vim.api.nvim_set_keymap('n', '<leader>dr', "<cmd>lua require'dap'.repl.open()<CR>", opts)
vim.api.nvim_set_keymap('n', '<leader>dl', "<cmd>lua require'dap'.run_last()<CR>", opts)
vim.api.nvim_set_keymap('n', '<leader>do', "<cmd>lua require'dapui'.open()<CR>", opts)
vim.api.nvim_set_keymap('n', '<leader>dc', "<cmd>lua require'dapui'.close()<CR>", opts)

-- remove trailing whitespace
-- also deletes trailing whitespace in multiline strings
-- vim.cmd [[ command! RemovePostspace execute '<cmd>:%s/\s\+$//e<cmd>' ]]

-- vim.api.nvim_set_keymap('n', '<leader>s', '<cmd>lua require("my-plugin").createFloatingWindow().onResize()<CR>', opts)
