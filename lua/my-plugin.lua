-- METATABLE-BASED CLASS
local MyClass = {} -- the table representing the class, which will double as the metatable for instances
MyClass.__index = MyClass -- failed table lookups on the instances should fallback to the class table, to get methods

-- syntax equivalent to "MyClass.new = function..."
function MyClass.new(init)
  local self = setmetatable({}, MyClass)
  self.value = init
  return self
end

function MyClass.set_value(self, newval)
  self.value = newval
end

function MyClass.get_value(self)
  return self.value
end

local i = MyClass.new(5)
-- tbl:name(arg) is a shortcut for tbl.name(tbl, arg), except tbl is evaluated only once
print(i:get_value()) --> 5
i:set_value(7)
print(i:get_value()) --> 7



-- Create a floating window
local function createFloatingWindow()
  local stats = vim.api.nvim_list_uis()[1]
  local width = stats.width
  local height = stats.height

  print("Window size", width, height)

  local bufh = vim.api.nvim_create_buf(false, true)
  local winid = vim.api.nvim_open_win(bufh, true, {
    relative="editor",
    width = width - 4,
    height = height - 4,
    col = 2,
    row = 2,
  })
end

local function onResize()
  local stats = vim.api.nvim_list_uis()[1]
  local width = stats.width
  local height = stats.height

  print("Window size", width, height)
end

return {
  createFloatingWindow = createFloatingWindow,
  onResize = onResize
}
