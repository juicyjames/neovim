-- get the current nvim buffer
local bufnr = vim.api.nvim_get_current_buf()
-- get the current nvim window
local win = vim.api.nvim_get_current_win()

-- vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, { "hello", "world" })

-- vim.api.nvim_open_win(bufnr, true,
--   {relative='cursor', row=3, col=3, width=12, height=3})

vim.api.nvim_create_autocmd("BufWritePost", {

  group = vim.api.nvim_create_augroup("MyAuGroup", { clear = true }),

  pattern = "server.py",

  callback = function()
    print("wow")
    vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, { "output of: file.py" })
    vim.fn.jobstart({"python", "server.py"}, {
      -- send full lines of output one at a time
      stdout_bufferd = true,

      -- if data append to buffer
      on_stdout = function(_, data)
        if data then
          vim.api.nvim_buf_set_lines(bufnr, -1, -1, false, data)
        end
      end,

      on_stderr = function(_, data)
        if data then
          vim.api.nvim_buf_set_lines(bufnr, -1, -1, false, data)
        end
      end
    })
  end
})

vim.api.nvim_create_autocmd("BufEnter", {

  group = vim.api.nvim_create_augroup("newGroup", { clear = true}),

  callback = function()
    print('Buffer number', vim.api.nvim_get_current_buf())
  end
})
